# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- handling of nested models in parameters (manualy tested only for body as its the only place it makes sense but supported everywhere anyway)

### Changed

- 

### Removed

- 

## [0.2.0] - 2024-03-26

### Added

- support for providing default values to parameter at expose app level
- AuthenticationResolver server_url is set before each call ensuring it is defined even if not specified when exposing the app

### Changed

- make child params of a Model option even if required

### Removed

- AuthenticationResolverProtocol, use the base class instead


## [0.1.1] - 2024-03-15

### Added

- generate cli from fastapi app
