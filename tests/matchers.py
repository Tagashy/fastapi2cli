import typing


class Any_[T]:
    def __init__(self, cls: type[T]):
        self.cls = cls

    def __eq__(self, other: typing.Any) -> bool:
        return isinstance(other, self.cls)

    def __str__(self):
        return f"{self.__class__.__name__}({self.cls.__name__})"

    def __repr__(self):
        return str(self)


class Or_:
    def __init__(self, *values):
        self.values = values

    def __eq__(self, other):
        return other in self.values


class Not_[T]:
    def __init__(self, value: T):
        self.value = value

    def __eq__(self, other: typing.Any) -> bool:
        return other != self.value


class ObjectWith:
    def __init__(self, **properties_to_validate: typing.Any):
        self.properties_to_validate = properties_to_validate

    def __eq__(self, other):
        return all(getattr(other, key) == value for key, value in self.properties_to_validate.items())


class InstanceOfWith[T](Any_, ObjectWith):
    def __init__(self, cls: type[T], **properties_to_validate: typing.Any):
        Any_.__init__(self, cls)
        ObjectWith.__init__(self, **properties_to_validate)

    def __eq__(self, other):
        return Any_.__eq__(self, other) and ObjectWith.__eq__(self, other)
