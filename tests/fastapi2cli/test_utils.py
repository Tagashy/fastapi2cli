from typing import Optional, Union

import pytest
from fastapi import FastAPI
from fastapi.routing import APIRoute
from fastapi2cli.utils import get_route_for_function, get_optional_type_value, is_optional


# Sample function for testing
def sample_function():
    pass


# Create a FastAPI app with a sample route
app = FastAPI()


@app.get("/")
def sample_route():
    pass


def test_get_route_for_function_exists():
    route = get_route_for_function(app, sample_route)
    assert isinstance(route, APIRoute)
    assert route.endpoint == sample_route


def test_get_route_for_function_not_exists():
    with pytest.raises(ValueError):
        get_route_for_function(app, sample_function)


def test_get_optional_type_value_optional_type():
    optional_type = Optional[int]
    result = get_optional_type_value(optional_type)
    assert result == int


def test_get_optional_type_value_union_type():
    union_type = Union[str, None]
    result = get_optional_type_value(union_type)
    assert result == str


def test_get_optional_type_value_invalid_input():
    with pytest.raises(TypeError):
        get_optional_type_value(int)


def test_is_optional_optional_type():
    assert is_optional(Optional[str]) is True


def test_is_optional_union_type():
    assert is_optional(Union[int, None]) is True


def test_is_optional_non_optional_type():
    assert is_optional(str) is False
