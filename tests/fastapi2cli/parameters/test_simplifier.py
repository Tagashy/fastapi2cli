from typing import Optional

from pydantic import BaseModel
from pydantic.fields import FieldInfo

from fastapi2cli.parameters.simplifier import explode_parameter, convert_parameters_to_typer_supported_format
from tests.matchers import InstanceOfWith

MODULE_PACKAGE = "fastapi2cli.parameters.simplifier"


class ChildModel(BaseModel):
    id: int


class ParentModel(BaseModel):
    name: str
    child: ChildModel
    age: int | None


class OtherModel(BaseModel):
    score: int


def test_explode_parameter_should_explode_nested_parameters():
    result = explode_parameter("parent", ParentModel)
    assert result == {
        "parent_name": InstanceOfWith(FieldInfo, annotation=str),
        "parent_child_id": InstanceOfWith(FieldInfo, annotation=int),
        "parent_age": InstanceOfWith(FieldInfo, annotation=Optional[int])
    }


def test_convert_parameters_to_typer_supported_format_should_return_params_as_is(mocker):
    explode_parameter_patch = mocker.patch(f'{MODULE_PACKAGE}.explode_parameter')
    validate_parameter_consistency_patch = mocker.patch(f'{MODULE_PACKAGE}.validate_parameter_consistency')
    # when
    result = convert_parameters_to_typer_supported_format({
        "name": FieldInfo(annotation=str),
        "child_id": FieldInfo(annotation=int),
        "age": FieldInfo(annotation=Optional[int]),
    })
    # then
    assert result == {
        "name": InstanceOfWith(FieldInfo, annotation=str),
        "child_id": InstanceOfWith(FieldInfo, annotation=int),
        "age": InstanceOfWith(FieldInfo, annotation=Optional[int]),
    }
    assert not explode_parameter_patch.called
    assert not validate_parameter_consistency_patch.called


def test_convert_parameters_to_typer_supported_format_should_return_exploded_parameters(mocker):
    explode_parameter_patch = mocker.patch(f'{MODULE_PACKAGE}.explode_parameter', return_value={
        "child_id": FieldInfo(annotation=int),
    })
    validate_parameter_consistency_patch = mocker.patch(f'{MODULE_PACKAGE}.validate_parameter_consistency')
    # when
    result = convert_parameters_to_typer_supported_format({
        "child": FieldInfo(annotation=ChildModel)
    })
    # then
    assert result == {
        "child_id": InstanceOfWith(FieldInfo, annotation=int),
    }
    explode_parameter_patch.assert_called_once_with("-child", ChildModel)
    validate_parameter_consistency_patch.assert_called_once_with("child_id", InstanceOfWith(FieldInfo, annotation=int),
                                                                 result)
