from typing import Annotated, Optional

import pytest
from fastapi import Header, Cookie
from fastapi.routing import APIRoute
from pydantic import BaseModel
from pydantic.fields import FieldInfo
from pydantic_core import PydanticUndefined

from fastapi2cli.web.parameters import (
    get_parameter_value,
    get_body,
    get_parameters_values,
    get_formatted_url,
    get_query_parameters,
    get_header_parameters,
    get_cookie_parameters,
    get_parameter_value_for_object
)


def test_get_parameter_value_with_required_field_missing_should_raise_key_error():
    # then
    with pytest.raises(KeyError):
        # when
        get_parameter_value("test", FieldInfo(annotation=str, default=PydanticUndefined), **{})


def test_get_parameter_value_with_optional_field_missing_should_return_default():
    default_value = "default"
    # when
    result = get_parameter_value("test", FieldInfo(annotation=str, default=default_value), **{})
    # then
    assert result == default_value


def test_get_parameter_value_with_optional_field_missing_should_return_default_factory():
    default_value = "default"
    # when
    result = get_parameter_value("test", FieldInfo(annotation=str, default_factory=lambda: default_value), **{})
    # then
    assert result == default_value


def test_get_parameter_value_with_required_field_present_should_return_value():
    value = "test_value"
    # when
    result = get_parameter_value("test", FieldInfo(annotation=str), test=value)
    # then
    assert result == value


def test_get_body_with_route_without_body_field_should_return_none():
    route = APIRoute("/", lambda: None)
    # when
    result = get_body(route)
    # then
    assert result is None


def test_get_body_with_route_with_body_field_should_return_body():
    class TestModel(BaseModel):
        test: str

    def fun(data: TestModel):
        pass

    route = APIRoute("/", fun, methods=["POST"])
    http_parameters = {"data_test": "value"}
    # when
    result = get_body(route, **http_parameters)
    # then
    assert isinstance(result, TestModel)
    assert result.test == "value"


def test_get_body_with_route_with_body_field_should_return_body_without_optional_not_specified():
    class TestModel(BaseModel):
        test: str
        provided_optional: Optional[str]
        unprovided_optional: str | None = None

    def fun(data: TestModel):
        pass

    route = APIRoute("/", fun, methods=["POST"])
    http_parameters = {"data_test": "value", "data_provided_optional": "option"}
    # when
    result = get_body(route, **http_parameters)
    # then
    assert isinstance(result, TestModel)
    assert result.test == "value"
    assert result.provided_optional == "option"
    assert result.unprovided_optional is None


def test_get_parameters_values_should_return_expected_values():
    parameter_definitions = {
        "test": FieldInfo(annotation=str),
        "optional_test": FieldInfo(annotation=str, default="default"),
        "required_test": FieldInfo(annotation=str)
    }
    http_parameters = {"test": "value", "required_test": "required"}
    # when
    result = get_parameters_values(parameter_definitions, **http_parameters)
    # then
    assert result == {"test": "value", "optional_test": "default", "required_test": "required"}


def test_get_formatted_url_should_return_expected_url():
    def fun(param: str):
        pass

    route = APIRoute("/{param}", fun)
    url = "http://example.com/"
    http_parameters = {"param": "value"}
    # when
    result = get_formatted_url(url, route, **http_parameters)
    # then
    assert result == "http://example.com/value"


def test_get_query_parameters_should_return_expected_parameters():
    def fun(param: str):
        pass

    route = APIRoute("/", fun)
    http_parameters = {"param": "value"}
    # when
    result = get_query_parameters(route, **http_parameters)
    # then
    assert result == {"param": "value"}


def test_get_header_parameters_should_return_expected_parameters():
    def fun(param: Annotated[str, Header(alias="X-Param")]):
        pass

    route = APIRoute("/", fun)
    http_parameters = {"X-Param": "value"}
    # when
    result = get_header_parameters(route, **http_parameters)
    # then
    assert result == {"X-Param": "value"}


def test_get_cookie_parameters_should_return_expected_parameters():
    def fun(session_id: Annotated[str, Cookie()]):
        pass

    route = APIRoute("/", fun)
    http_parameters = {"session_id": "value"}
    # when
    result = get_cookie_parameters(route, **http_parameters)
    # then
    assert result == {"session_id": "value"}


# Define a sample Pydantic model for testing
class TestModel(BaseModel):
    required_field: str
    optional_field: Optional[str] = None


def test_get_parameter_value_with_nested_object_should_call_get_parameter_value_for_object(mocker):
    value = "test_value"
    get_parameter_value_for_object_patch = mocker.patch("fastapi2cli.web.parameters.get_parameter_value_for_object")
    # when
    result = get_parameter_value("test", FieldInfo(annotation=TestModel), test_required_field=value)
    # then
    assert result == get_parameter_value_for_object_patch.return_value
    get_parameter_value_for_object_patch.assert_called_once_with(TestModel, "test", test_required_field=value)


def test_get_parameter_value_for_object_with_all_fields_present():
    # given
    http_parameters = {
        "test_required_field": "required_value",
        "test_optional_field": "optional_value"
    }
    # when
    result = get_parameter_value_for_object(TestModel, "test", **http_parameters)
    # then
    assert isinstance(result, TestModel)
    assert result.required_field == "required_value"
    assert result.optional_field == "optional_value"


def test_get_parameter_value_for_object_with_missing_optional_field():
    # given
    http_parameters = {
        "test_required_field": "required_value"
    }
    # when
    result = get_parameter_value_for_object(TestModel, "test", **http_parameters)
    # then
    assert isinstance(result, TestModel)
    assert result.required_field == "required_value"
    assert result.optional_field is None


def test_get_parameter_value_for_object_with_missing_required_field_should_raise_key_error():
    # given
    http_parameters = {
        "test_optional_field": "optional_value"
    }
    # then
    with pytest.raises(KeyError):
        # when
        get_parameter_value_for_object(TestModel, "test", **http_parameters)


def test_get_parameter_value_for_object_with_default_values():
    class TestModelWithDefaults(BaseModel):
        required_field: str
        optional_field: Optional[str] = "default_value"

    # given
    http_parameters = {
        "test_required_field": "required_value"
    }
    # when
    result = get_parameter_value_for_object(TestModelWithDefaults, "test", **http_parameters)
    # then
    assert isinstance(result, TestModelWithDefaults)
    assert result.required_field == "required_value"
    assert result.optional_field == "default_value"


def test_get_parameter_value_for_object_with_nested_field_names():
    class NestedModel(BaseModel):
        nested_field: str

    class OuterModel(BaseModel):
        outer_field: str
        nested_model: NestedModel

    # given
    http_parameters = {
        "test_outer_field": "outer_value",
        "test_nested_model_nested_field": "nested_value"
    }
    # when
    result = get_parameter_value_for_object(OuterModel, "test", **http_parameters)
    # then
    assert isinstance(result, OuterModel)
    assert result.outer_field == "outer_value"
    assert isinstance(result.nested_model, NestedModel)
    assert result.nested_model.nested_field == "nested_value"
