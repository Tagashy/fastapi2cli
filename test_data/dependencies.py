from typing import Annotated
from typing import AsyncGenerator
from uuid import UUID

from fastapi import Depends

from test_data.classes import AsyncEngine, get_engine, async_sessionmaker, get_session_factory, AsyncSession, \
    HostRepository, HostService, ConnectionManager, CustomerRepository, CustomerService


def get_session_maker(engine: Annotated[AsyncEngine, Depends(get_engine)]) -> async_sessionmaker:
    return get_session_factory(engine)


async def get_db_session(
        session_maker: Annotated[async_sessionmaker, Depends(get_session_maker)]
) -> AsyncGenerator[AsyncSession, None]:
    session = session_maker()
    async with session.begin():
        yield session


async def get_host_repository(session: Annotated[AsyncSession, Depends(get_db_session)],
                              project_id: UUID) -> HostRepository:
    return HostRepository(session, project_id=project_id)


async def get_host_service(repository: Annotated[HostRepository, Depends(get_host_repository)],
                           connection_manager: Annotated[
                               ConnectionManager, Depends(ConnectionManager.get_instance)]) -> HostService:
    return HostService(repository, connection_manager)


async def get_customer_repository(session: Annotated[AsyncSession, Depends(get_db_session)]) -> CustomerRepository:
    return CustomerRepository(session)


async def get_customer_service(
        repository: Annotated[CustomerRepository, Depends(get_customer_repository)]) -> CustomerService:
    return CustomerService(repository)
